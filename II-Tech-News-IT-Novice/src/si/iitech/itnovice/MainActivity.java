package si.iitech.itnovice;

import java.util.ArrayList;
import java.util.List;

import si.iiteam.tehnoloskenovice.R;
import si.iitech.news.activity.ArticleListActivity;
import si.iitech.news.fragments.ArticleFragmentHolder;

public class MainActivity extends ArticleListActivity {

	private String	rest						= "http://95.85.43.192:8080/II-Tech/service/article/";
	private int		type						= 0;

	private String	sloTechTitle				= "Slo Tech";
	private int		sloTechSource				= 0;

	private String	mobitelTehnikTitle			= "Mobitel Tehnik";
	private int		mobitelTehnikSource			= 1;

	private String	racunalniskeNoviceTitle		= "Računalniske novice";
	private int		racunalniskeNoviceSource	= 2;

	private String	gizzmoNoviceTitle			= "Gizzmo Novice";
	private int		gizzmoNoviceSource			= 3;

	private String	sloAndroidTitle				= "Slo Android";
	private int		sloAndroidSource			= 4;

	@Override
	public List<ArticleFragmentHolder> getArticleListFragments() {

		List<ArticleFragmentHolder> list = new ArrayList<ArticleFragmentHolder>();
		int request = super.getRequestID();

		ArticleFragmentHolder news = ArticleFragmentHolder.createHolderForLatestArticleListFragment(this,
				this.getResources().getDrawable(R.drawable.latest_news), rest, request, type);
		list.add(news);
		news = ArticleFragmentHolder.createHolderForArticleListFragment(this, this.getResources().getDrawable(R.drawable.slo_tech), sloTechTitle, rest,
				request, type, sloTechSource);
		list.add(news);
		news = ArticleFragmentHolder.createHolderForArticleListFragment(this, this.getResources().getDrawable(R.drawable.mobitel_tehnik), mobitelTehnikTitle,
				rest, request, type, mobitelTehnikSource);
		list.add(news);
		news = ArticleFragmentHolder.createHolderForArticleListFragment(this, this.getResources().getDrawable(R.drawable.racunalniske_novice),
				racunalniskeNoviceTitle, rest, request, type, racunalniskeNoviceSource);
		list.add(news);
		news = ArticleFragmentHolder.createHolderForArticleListFragment(this, this.getResources().getDrawable(R.drawable.gizzmo_novice), gizzmoNoviceTitle,
				rest, request, type, gizzmoNoviceSource);
		list.add(news);
		news = ArticleFragmentHolder.createHolderForArticleListFragment(this, this.getResources().getDrawable(R.drawable.slo_android), sloAndroidTitle, rest,
				request, type, sloAndroidSource);
		list.add(news);
		return list;
	}

	@Override
	public ArticleFragmentHolder getArticleSearchFragments() {
		return ArticleFragmentHolder.createHolderForArticleSearchFragment(this, rest, super.getRequestID(), type);
	}

}
