/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package si.iitech.news;

public final class R {
	public static final class anim {
		public static final int slide_in_from_bottom = 0x7f040000;
		public static final int slide_in_from_top = 0x7f040001;
		public static final int slide_out_to_bottom = 0x7f040002;
		public static final int slide_out_to_top = 0x7f040003;
	}
	public static final class attr {
		public static final int behindOffset = 0x7f010028;
		public static final int behindScrollScale = 0x7f01002a;
		public static final int behindWidth = 0x7f010029;
		public static final int fadeDegree = 0x7f010030;
		public static final int fadeEnabled = 0x7f01002f;
		public static final int mode = 0x7f010025;
		public static final int ptrAdapterViewBackground = 0x7f010022;
		public static final int ptrAnimationStyle = 0x7f01001e;
		public static final int ptrDrawable = 0x7f010018;
		public static final int ptrDrawableBottom = 0x7f010024;
		public static final int ptrDrawableEnd = 0x7f01001a;
		public static final int ptrDrawableStart = 0x7f010019;
		public static final int ptrDrawableTop = 0x7f010023;
		public static final int ptrHeaderBackground = 0x7f010013;
		public static final int ptrHeaderSubTextColor = 0x7f010015;
		public static final int ptrHeaderTextAppearance = 0x7f01001c;
		public static final int ptrHeaderTextColor = 0x7f010014;
		public static final int ptrListViewExtrasEnabled = 0x7f010020;
		public static final int ptrMode = 0x7f010016;
		public static final int ptrOverScroll = 0x7f01001b;
		public static final int ptrRefreshableViewBackground = 0x7f010012;
		public static final int ptrRotateDrawableWhilePulling = 0x7f010021;
		public static final int ptrScrollingWhileRefreshingEnabled = 0x7f01001f;
		public static final int ptrShowIndicator = 0x7f010017;
		public static final int ptrSubHeaderTextAppearance = 0x7f01001d;
		public static final int selectorDrawable = 0x7f010032;
		public static final int selectorEnabled = 0x7f010031;
		public static final int shadowDrawable = 0x7f01002d;
		public static final int shadowWidth = 0x7f01002e;
		public static final int touchModeAbove = 0x7f01002b;
		public static final int touchModeBehind = 0x7f01002c;
		public static final int viewAbove = 0x7f010026;
		public static final int viewBehind = 0x7f010027;
	}
	public static final class color {
		public static final int bela = 0x7f07000a;
		public static final int black = 0x7f070009;
		public static final int fb_modra_napis = 0x7f07000f;
		public static final int fb_siva = 0x7f07000e;
		public static final int gray = 0x7f070016;
		public static final int modra = 0x7f070013;
		public static final int nina_bez = 0x7f070014;
		public static final int opozorilo = 0x7f07000b;
		public static final int prodajalna_ozadje = 0x7f070010;
		public static final int prodajalna_ozadje_klik = 0x7f070011;
		public static final int rdeca = 0x7f070007;
		public static final int red = 0x7f070015;
		public static final int siva_okras = 0x7f070012;
		public static final int svetlo_siva = 0x7f07000d;
		public static final int transparentna = 0x7f07000c;
		public static final int zelena = 0x7f070008;
	}
	public static final class dimen {
		public static final int gallery_title_size = 0x7f060014;
		public static final int header_footer_left_right_padding = 0x7f060010;
		public static final int header_footer_top_bottom_padding = 0x7f060011;
		public static final int indicator_corner_radius = 0x7f06000e;
		public static final int indicator_internal_padding = 0x7f06000f;
		public static final int indicator_right_padding = 0x7f06000d;
		public static final int menu_text_size = 0x7f060012;
		public static final int normal_text_size = 0x7f060016;
		public static final int padding = 0x7f060015;
		public static final int small_text_size = 0x7f060017;
		public static final int text_margin = 0x7f06001a;
		public static final int text_size_18 = 0x7f060018;
		public static final int text_size_20 = 0x7f060019;
		public static final int title_size = 0x7f060013;
	}
	public static final class drawable {
		public static final int about = 0x7f020000;
		public static final int button_background_type_1 = 0x7f020001;
		public static final int button_background_type_1_click = 0x7f020002;
		public static final int button_background_type_1_no_click = 0x7f020003;
		public static final int default_ptr_flip = 0x7f020024;
		public static final int default_ptr_rotate = 0x7f020025;
		public static final int ic_launcher = 0x7f020027;
		public static final int icon_big = 0x7f020028;
		public static final int icon_ii_tech_big = 0x7f020029;
		public static final int indicator_arrow = 0x7f02002a;
		public static final int indicator_bg_bottom = 0x7f02002b;
		public static final int indicator_bg_top = 0x7f02002c;
		public static final int no_picture_background = 0x7f02002f;
		public static final int search = 0x7f020031;
		public static final int settings = 0x7f020032;
		public static final int shadow = 0x7f020033;
		public static final int share = 0x7f020034;
	}
	public static final class id {
		public static final int LinearLayout = 0x7f0a0014;
		public static final int ScrollView = 0x7f0a001a;
		public static final int TextView_videos_title = 0x7f0a0023;
		public static final int ViewSwitcher = 0x7f0a0028;
		public static final int add = 0x7f0a004a;
		public static final int both = 0x7f0a0007;
		public static final int disabled = 0x7f0a0008;
		public static final int fl_inner = 0x7f0a0044;
		public static final int flip = 0x7f0a000e;
		public static final int fullscreen = 0x7f0a0012;
		public static final int gridview = 0x7f0a0000;
		public static final int horizontalScrollView_images = 0x7f0a0021;
		public static final int horizontalScrollView_videos = 0x7f0a0024;
		public static final int imageView_article_image = 0x7f0a0040;
		public static final int imageView_image = 0x7f0a002a;
		public static final int image_title = 0x7f0a0043;
		public static final int imgDisplay = 0x7f0a0042;
		public static final int left = 0x7f0a0010;
		public static final int linearLayout_images = 0x7f0a0022;
		public static final int linearLayout_sources = 0x7f0a0027;
		public static final int linearLayout_videos = 0x7f0a0025;
		public static final int listView_result = 0x7f0a004b;
		public static final int manualOnly = 0x7f0a0009;
		public static final int margin = 0x7f0a0013;
		public static final int menu_item_share = 0x7f0a004e;
		public static final int pager = 0x7f0a0019;
		public static final int pullDownFromTop = 0x7f0a000a;
		public static final int pullFromEnd = 0x7f0a000b;
		public static final int pullFromStart = 0x7f0a000c;
		public static final int pullUpFromBottom = 0x7f0a000d;
		public static final int pull_to_refresh_image = 0x7f0a0045;
		public static final int pull_to_refresh_progress = 0x7f0a0046;
		public static final int pull_to_refresh_sub_text = 0x7f0a0048;
		public static final int pull_to_refresh_text = 0x7f0a0047;
		public static final int right = 0x7f0a0011;
		public static final int rotate = 0x7f0a000f;
		public static final int scrollview = 0x7f0a0002;
		public static final int selected_view = 0x7f0a0003;
		public static final int slidingmenumain = 0x7f0a004c;
		public static final int text = 0x7f0a0049;
		public static final int textView_article_image_title = 0x7f0a0041;
		public static final int textView_author = 0x7f0a001c;
		public static final int textView_contact = 0x7f0a0017;
		public static final int textView_data = 0x7f0a001e;
		public static final int textView_date = 0x7f0a001d;
		public static final int textView_ii_tech = 0x7f0a0016;
		public static final int textView_images_title = 0x7f0a0020;
		public static final int textView_link = 0x7f0a001f;
		public static final int textView_mai = 0x7f0a0018;
		public static final int textView_owner = 0x7f0a0015;
		public static final int textView_sources_title = 0x7f0a0026;
		public static final int textView_title = 0x7f0a001b;
		public static final int view_articles = 0x7f0a0029;
		public static final int webview = 0x7f0a0001;
	}
	public static final class layout {
		public static final int about = 0x7f030000;
		public static final int activity_fullscreen_view = 0x7f030001;
		public static final int article_details_activity = 0x7f030003;
		public static final int article_list_activity = 0x7f030004;
		public static final int article_preview_adapter = 0x7f030005;
		public static final int full_size_image_dialog = 0x7f030014;
		public static final int layout_fullscreen_image = 0x7f030015;
		public static final int need_this_for_maven = 0x7f030016;
		public static final int pull_to_refresh_header_horizontal = 0x7f030017;
		public static final int pull_to_refresh_header_vertical = 0x7f030018;
		public static final int search = 0x7f030019;
		public static final int slidingmenumain = 0x7f03001a;
	}
	public static final class menu {
		public static final int share = 0x7f0b0001;
	}
	public static final class string {
		public static final int about = 0x7f080033;
		public static final int app_name = 0x7f08001f;
		public static final int article_image_gallery = 0x7f080041;
		public static final int article_share = 0x7f080048;
		public static final int article_sources = 0x7f080043;
		public static final int article_video = 0x7f080044;
		public static final int article_video_gallery = 0x7f080042;
		public static final int bundle_request = 0x7f08004b;
		public static final int bundle_rest = 0x7f080049;
		public static final int bundle_title = 0x7f08004a;
		public static final int bundle_type = 0x7f08004c;
		public static final int contact = 0x7f08004f;
		public static final int developerName = 0x7f080038;
		public static final int ii_tech = 0x7f08004e;
		public static final int latest_news = 0x7f080045;
		public static final int mail = 0x7f080050;
		public static final int nameData = 0x7f080035;
		public static final int nameIgor = 0x7f080039;
		public static final int nameIrena = 0x7f08003a;
		public static final int nameTitle = 0x7f080034;
		public static final int opozorilo_ni_podatkov = 0x7f08003d;
		public static final int opozorilo_ni_podatkov_iskanje = 0x7f08003c;
		public static final int opozorilo_ni_vnesenih_podatkov = 0x7f08003e;
		public static final int owner = 0x7f08004d;
		public static final int popup_article_item1 = 0x7f080020;
		public static final int pull_to_refresh_from_bottom_pull_label = 0x7f08001c;
		public static final int pull_to_refresh_from_bottom_refreshing_label = 0x7f08001e;
		public static final int pull_to_refresh_from_bottom_release_label = 0x7f08001d;
		public static final int pull_to_refresh_pull_label = 0x7f080019;
		public static final int pull_to_refresh_refreshing_label = 0x7f08001b;
		public static final int pull_to_refresh_release_label = 0x7f08001a;
		public static final int rest_mobi_novice = 0x7f080025;
		public static final int rest_mobi_novice_title = 0x7f08002c;
		public static final int rest_mobitel_tehnik = 0x7f080022;
		public static final int rest_mobitel_tehnik_title = 0x7f080029;
		public static final int rest_racunalniske_novice = 0x7f080023;
		public static final int rest_racunalniske_novice_title = 0x7f08002a;
		public static final int rest_search = 0x7f080027;
		public static final int rest_search_title = 0x7f08002e;
		public static final int rest_slo_android = 0x7f080026;
		public static final int rest_slo_android_title = 0x7f08002d;
		public static final int rest_slo_tech = 0x7f080021;
		public static final int rest_slo_tech_title = 0x7f080028;
		public static final int rest_zadnje_novice = 0x7f080024;
		public static final int rest_zadnje_novice_title = 0x7f08002b;
		public static final int sample_date = 0x7f080040;
		public static final int sample_text = 0x7f08003f;
		public static final int search = 0x7f080031;
		public static final int search_hint = 0x7f08002f;
		public static final int search_label = 0x7f080030;
		public static final int settings = 0x7f080032;
		public static final int share_title = 0x7f08003b;
		public static final int versionData = 0x7f080037;
		public static final int versionTitle = 0x7f080036;
		public static final int warning_no_data_search = 0x7f080047;
		public static final int warning_no_data_transfer = 0x7f080046;
	}
	public static final class style {
		public static final int AppBaseTheme = 0x7f090002;
		public static final int AppTheme = 0x7f090003;
		public static final int NormalText = 0x7f090007;
		public static final int PreviewTitle = 0x7f090006;
		public static final int SmallText = 0x7f090008;
		public static final int Title = 0x7f090004;
		public static final int TitleGallery = 0x7f090005;
		public static final int imageTitleStyle = 0x7f090009;
		public static final int menu_text = 0x7f09000a;
		public static final int normal_bold_text = 0x7f09000c;
		public static final int normal_text = 0x7f09000b;
	}
	public static final class styleable {
		public static final int[] PullToRefresh = { 0x7f010012, 0x7f010013, 0x7f010014, 0x7f010015, 0x7f010016, 0x7f010017, 0x7f010018, 0x7f010019, 0x7f01001a, 0x7f01001b, 0x7f01001c, 0x7f01001d, 0x7f01001e, 0x7f01001f, 0x7f010020, 0x7f010021, 0x7f010022, 0x7f010023, 0x7f010024 };
		public static final int PullToRefresh_ptrAdapterViewBackground = 16;
		public static final int PullToRefresh_ptrAnimationStyle = 12;
		public static final int PullToRefresh_ptrDrawable = 6;
		public static final int PullToRefresh_ptrDrawableBottom = 18;
		public static final int PullToRefresh_ptrDrawableEnd = 8;
		public static final int PullToRefresh_ptrDrawableStart = 7;
		public static final int PullToRefresh_ptrDrawableTop = 17;
		public static final int PullToRefresh_ptrHeaderBackground = 1;
		public static final int PullToRefresh_ptrHeaderSubTextColor = 3;
		public static final int PullToRefresh_ptrHeaderTextAppearance = 10;
		public static final int PullToRefresh_ptrHeaderTextColor = 2;
		public static final int PullToRefresh_ptrListViewExtrasEnabled = 14;
		public static final int PullToRefresh_ptrMode = 4;
		public static final int PullToRefresh_ptrOverScroll = 9;
		public static final int PullToRefresh_ptrRefreshableViewBackground = 0;
		public static final int PullToRefresh_ptrRotateDrawableWhilePulling = 15;
		public static final int PullToRefresh_ptrScrollingWhileRefreshingEnabled = 13;
		public static final int PullToRefresh_ptrShowIndicator = 5;
		public static final int PullToRefresh_ptrSubHeaderTextAppearance = 11;
		public static final int[] SlidingMenu = { 0x7f010025, 0x7f010026, 0x7f010027, 0x7f010028, 0x7f010029, 0x7f01002a, 0x7f01002b, 0x7f01002c, 0x7f01002d, 0x7f01002e, 0x7f01002f, 0x7f010030, 0x7f010031, 0x7f010032 };
		public static final int SlidingMenu_behindOffset = 3;
		public static final int SlidingMenu_behindScrollScale = 5;
		public static final int SlidingMenu_behindWidth = 4;
		public static final int SlidingMenu_fadeDegree = 11;
		public static final int SlidingMenu_fadeEnabled = 10;
		public static final int SlidingMenu_mode = 0;
		public static final int SlidingMenu_selectorDrawable = 13;
		public static final int SlidingMenu_selectorEnabled = 12;
		public static final int SlidingMenu_shadowDrawable = 8;
		public static final int SlidingMenu_shadowWidth = 9;
		public static final int SlidingMenu_touchModeAbove = 6;
		public static final int SlidingMenu_touchModeBehind = 7;
		public static final int SlidingMenu_viewAbove = 1;
		public static final int SlidingMenu_viewBehind = 2;
	}
	public static final class xml {
		public static final int searchable_configuration = 0x7f050000;
		public static final int settings = 0x7f050001;
		public static final int widget = 0x7f050002;
	}
}
