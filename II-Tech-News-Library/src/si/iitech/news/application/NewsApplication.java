package si.iitech.news.application;

import si.iitech.news.R;
import android.app.Application;
import android.util.Log;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class NewsApplication extends Application {
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		DisplayImageOptions options = new DisplayImageOptions.Builder()
		.showStubImage(R.drawable.no_picture_background)
		.showImageForEmptyUri(R.drawable.no_picture_background)
		.showImageOnFail(R.drawable.no_picture_background)
		
		.resetViewBeforeLoading().delayBeforeLoading(0).cacheInMemory()
		.cacheOnDisc()
		.displayer(new FadeInBitmapDisplayer(500))
		.build();
		
		
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				this)
	    	.threadPoolSize(3)
	    	.defaultDisplayImageOptions(options)
			.build();
		ImageLoader.getInstance().init(config);
		Log.i("Application", "onCreate");
	}

}
