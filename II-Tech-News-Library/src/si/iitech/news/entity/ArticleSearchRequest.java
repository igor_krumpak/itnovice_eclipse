package si.iitech.news.entity;

import com.google.gson.Gson;

public class ArticleSearchRequest {

	private int			request;
	private int			type;
	private String[]	keyWords;

	public int getRequest() {
		return request;
	}

	public void setRequest(int request) {
		this.request = request;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String[] getKeyWords() {
		return keyWords;
	}

	public void setKeyWords(String[] keyWords) {
		this.keyWords = keyWords;
	}

	public String toJsonString() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}
}
