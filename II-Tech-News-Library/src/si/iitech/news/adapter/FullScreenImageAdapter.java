package si.iitech.news.adapter;

import java.util.ArrayList;

import si.iitech.news.R;
import si.iitech.util.TouchImageView;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class FullScreenImageAdapter extends PagerAdapter {

	private Context				context;
	private ArrayList<String>	imagePaths;
	private ArrayList<String>	imageTitles;
	private LayoutInflater		inflater;
	private DisplayImageOptions	options;

	// constructor
	public FullScreenImageAdapter(Context context, ArrayList<String> imagePaths, ArrayList<String> imageTitles) {

		this.context = context;
		this.imagePaths = imagePaths;
		this.imageTitles = imageTitles;
	}

	@Override
	public int getCount() {
		return this.imagePaths.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((LinearLayout) object);
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		TouchImageView imgDisplay;
		TextView imageTitle;

		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container, false);

		imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.imgDisplay);
		// imgDisplay.setScaleType(ScaleType.CENTER);
		imageTitle = (TextView) viewLayout.findViewById(R.id.image_title);
		ImageLoader.getInstance().displayImage(imagePaths.get(position), imgDisplay, options);

		if (!imageTitles.get(position).equalsIgnoreCase(null)) {
			imageTitle.setVisibility(View.VISIBLE);
			imageTitle.setText(imageTitles.get(position));
		}

		((ViewPager) container).addView(viewLayout);

		return viewLayout;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		((ViewPager) container).removeView((LinearLayout) object);
	}

}
