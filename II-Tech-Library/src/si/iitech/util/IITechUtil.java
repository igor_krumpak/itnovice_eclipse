package si.iitech.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class IITechUtil {

	public static String generateRandomUIID() {
		UUID uniqueKey = UUID.randomUUID();
		return uniqueKey.toString();
	}

	@SuppressLint("SimpleDateFormat")
	public static long convertTimeToMilis(String time, String format) {
		Date date;
		try {
			date = new SimpleDateFormat(format).parse(time);
			Log.d("IITECHUTIL CONVERTER", date.toString());
		} catch (ParseException e) {
			Log.d("IITECHUTIL CONVERTER", e.toString());
			return 0;
		}

		return date.getTime();
	}

	public static void hideKeyboard(Activity activity) {
		InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}

	public static long getCurrentTime() {
		Time t = new Time();
		t.setToNow();
		return t.toMillis(true);
	}

	public static String formatTimeToDMYhms(long time) {
		Time t = new Time();
		t.set(time);
		return t.format("%d.%m.%Y %H:%M:%S");
	}

	public static String formatTimeToDMY(long time) {
		Time t = new Time();
		t.set(time);
		return t.format("%d.%m.%Y");
	}

	public static <T> void startNewActivity(Class<T> target, final Activity activity) {
		Intent intent = new Intent(activity, target);
		activity.startActivity(intent);
	}

	public static int convertDpToPx(Context context, int dp) {
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		float fpixels = metrics.density * dp;
		int pixels = (int) (fpixels + 0.5f);
		return pixels;
	}

	public static void saveInSharePreferences(Activity activity, String appName, String key, String value) {
		SharedPreferences sharedPreferences = activity.getSharedPreferences(appName, Context.MODE_PRIVATE);
		SharedPreferences.Editor prefEditor = sharedPreferences.edit();
		prefEditor.putString(key, value);
		prefEditor.commit();
	}

	public static String retriveFromSharedPreferences(Activity activity, String appName, String key, String defaultValue) {
		SharedPreferences sharedPreferences = activity.getSharedPreferences(appName, Context.MODE_PRIVATE);
		return sharedPreferences.getString(key, defaultValue);
	}

	public static double[] locationToLonAndLat(Context context, String location) {
		double[] geoLocation = new double[2];
		try {
			String locationString = location;
			Geocoder coder = new Geocoder(context);
			List<Address> address = coder.getFromLocationName(locationString, 1);
			if (address != null) {
				Address temp = address.get(0);
				geoLocation[0] = temp.getLatitude();
				geoLocation[1] = temp.getLongitude();
			}
		} catch (Exception e) {
			Log.d("location error", e.toString());
		}
		return geoLocation;
	}
	
	public static boolean checkValues(EditText editText, String regex, String noValue, String wrongValue) {
		if (editText.getText().toString().length() == 0) {
			editText.setError(noValue);
			editText.requestFocus();
			return false;
		} else {
			editText.setError(null);
		}

		if (regex != null && regex.length() > 0) {
			if (!editText.getText().toString().matches(regex)) {
				editText.setError(wrongValue);
				editText.requestFocus();
				return false;
			} else {
				editText.setError(null);
			}
		}
		return true;
	}

	/**
	 * public static void log(Class<?> clazz, String TAG, String message) {
	 * Log.i(clazz.getName() + " " + TAG, message); }
	 * 
	 * public static void log(Class<?> clazz, String TAG, int value) {
	 * Log.i(clazz.getName() + " " + TAG, value + ""); }
	 * 
	 * public static void log(Class<?> clazz, String message) { log(clazz,
	 * message); }
	 * 
	 * public static void log(Class<?> clazz, int value) { log(clazz, value); }
	 */
}
